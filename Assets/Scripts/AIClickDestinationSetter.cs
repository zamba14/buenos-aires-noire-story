﻿using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[UniqueComponent(tag = "ai.destination")]
[RequireComponent(typeof(IAstarAI))]
public class AIClickDestinationSetter : MonoBehaviour
{
    private IAstarAI ai = default;
    private Vector3 destinationPosition = default;

    private void OnEnable()
    {
		ai = GetComponent<IAstarAI>();
        if (ai == null)
            throw new MissingComponentException(
                $"{gameObject.name} GameObject is missing an IAstarAI component for the AIClickDestinationSetter to work."
            );

        ai.onSearchPath += SetDestination;
    }

    private void OnDisable()
    {
        ai.onSearchPath -= SetDestination;
    }

    public void OnSetDestination(BaseEventData data)
    {
        PointerEventData pData = (PointerEventData) data;
        destinationPosition = pData.pointerCurrentRaycast.worldPosition;
        SetDestination();
    }

    private void SetDestination()
    {
        if (destinationPosition == null) {
            Debug.LogWarning($"DestinationPosition not set yet in AIClickDestinationSetter of {gameObject.name}", this);
            return;
        }

        ai.destination = destinationPosition;
    }
}
